py-trello==0.20.1
pytest
pytest-mock
pytest-cov
python-dotenv
pyfakefs
autopep8
tqdm
